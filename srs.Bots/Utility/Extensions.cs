﻿using System;
using System.Collections.Generic;
using System.Linq;
using srs.Bots.Data;

namespace srs.Bots.Utility
{
    /// <summary>
    /// public extensions
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// create new message based on existing one
        /// </summary>
        /// <param name="message">old message</param>
        /// <param name="format">format for text of new message, with single placeholder for old text</param>
        /// <param name="recipient">peer info</param>
        /// <param name="preserveOptions">true to pass options from old message to new</param>
        /// <param name="ctx">context to populate with options</param>
        /// <returns>new message</returns>
        public static PeerMessage Wrap(this PeerMessage message, string format, ChatInfo recipient = null, bool preserveOptions = false, CommandContext ctx = null)
        {
            if (message.IsFile) return message;
            var wrap = PeerMessage.FromText(string.Format(format, message.Message),
                recipient ?? new ChatInfo {Id = message.Recipient});
            if (preserveOptions) wrap.WithOptions(message.Options, ctx);
            return wrap;
        }
        /// <summary>
        /// fill context with attachments
        /// </summary>
        /// <param name="ctx">context to fill</param>
        /// <param name="attachments">attachments to put</param>
        /// <returns>the context passed</returns>
        public static CommandContext PutAttachments(this CommandContext ctx, IEnumerable<Attachment> attachments)
        {
            if (attachments is null) return ctx;
            foreach (var attachment in attachments)
                ctx.PushAttachment(attachment);
            return ctx;
        }
        /// <summary>
        /// Gets the keyboard for message.
        /// </summary>
        /// <typeparam name="T">button type</typeparam>
        /// <param name="message">The message.</param>
        /// <param name="selector">The selector from keyboard option to button.</param>
        /// <returns></returns>
        public static IEnumerable<T[]> GetKeyboard<T>(this PeerMessage message, Func<string, int, T> selector)
        {
            var options = message.Options;
            var offset = 0;
            var res = new List<T[]>();
            foreach (var row in message.ButtonsInRow)
            {
                var buttons = options.Skip(offset).Take(row);
                offset += row;
                res.Add(buttons.Select(selector).ToArray());
            }
            return res.Where(x => x.Length > 0);
        }
    }
}
