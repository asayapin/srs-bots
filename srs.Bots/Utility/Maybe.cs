﻿using System;

namespace srs.Bots.Utility
{
    /// <summary>
    /// nullable analog for reference types (with additional exception container)
    /// </summary>
    /// <typeparam name="T">any type</typeparam>
    public class Maybe<T>
    {
        /// <summary>
        /// Gets the exception.
        /// </summary>
        public Exception Exception { get; }
        /// <summary>
        /// Gets the value.
        /// </summary>
        public T Value { get; }
        /// <summary>
        /// Gets a value indicating whether this instance contains value.
        /// </summary>
        public bool HasValue { get; }
        /// <summary>
        /// Initializes a new instance of the <see cref="Maybe{T}"/> class.
        /// </summary>
        /// <param name="value">The value.</param>
        private Maybe(T value)
        {
            Value = value;
            HasValue = true;
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="Maybe{T}"/> class.
        /// </summary>
        /// <param name="ex">The ex.</param>
        private Maybe(Exception ex)
        {
            Exception = ex;
            HasValue = false;
        }
        /// <summary>
        /// wrap value in Maybe instance
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static Maybe<T> FromValue(T value) => new Maybe<T>(value);
        /// <summary>
        /// Wrap exception in new Maybe instance
        /// </summary>
        /// <param name="e">The e.</param>
        /// <returns></returns>
        public static Maybe<T> FromError(Exception e) => new Maybe<T>(e);
        /// <summary>
        /// empty maybe - in case no exception is thrown but result can't be provided
        /// </summary>
        public static Maybe<T> Empty { get; } = new Maybe<T>(null);
    }
}
