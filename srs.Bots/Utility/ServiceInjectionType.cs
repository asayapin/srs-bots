﻿namespace srs.Bots.Utility
{
    /// <summary>
    /// type of injection into service collection
    /// </summary>
    public enum ServiceInjectionType
    {
        /// <summary>
        /// inject using AddSingleton
        /// </summary>
        Singleton,
        /// <summary>
        /// Inject using AddScoped
        /// </summary>
        Scoped,
        /// <summary>
        /// inject using AddTransient
        /// </summary>
        Transient
    }
}
