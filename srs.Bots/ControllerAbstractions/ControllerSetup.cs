﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using srs.Bots.Data;
using srs.Bots.Utility;

namespace srs.Bots.ControllerAbstractions
{
    /// <summary>
    /// bot controller setup envelope
    /// </summary>
    public class ControllerSetup
    {
        /// <summary>
        /// POST request response factory
        /// </summary>
        public Func<IActionResult> RequestResult { get; internal set; }
        /// <summary>
        /// parser delegate
        /// </summary>
        public Func<JObject, Maybe<UnifiedMessage>> ParseRequest { get; internal set; }
        /// <summary>
        /// true to start from scratch on every new command
        /// </summary>
        public bool DropContextOnNewCommand { get; internal set; } = true;
        /// <summary>
        /// true to check every message to be present in context's options (if they're present)
        /// </summary>
        public bool ValidateInlineConformance { get; internal set; } = true;
        /// <summary>
        /// Determines whether this instance is fulfilled.
        /// </summary>
        internal bool IsFilled()
        {
            return !GetType().GetProperties().Any(x => x.GetValue(this) is null);
        }
    }
}
