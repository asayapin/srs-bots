﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using srs.Bots.CommandAbstractions;
using srs.Bots.Data;
using srs.Bots.Utility;

namespace srs.Bots.ControllerAbstractions
{
    /// <summary>
    /// base bot controller
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.ControllerBase" />
    public abstract class UnifiedController : ControllerBase 
    {
        private readonly ControllerSetup _setup;
        private static bool _isInit;
        /// <summary>
        /// The command executor
        /// </summary>
        protected readonly IExecutor Executor;
        /// <summary>
        /// The processed messages set, to prevent repetitive processing
        /// </summary>
        protected readonly HashSet<long> ProcessedMessages = new HashSet<long>();

        private static readonly ConcurrentDictionary<long, CommandContext> Contexts = new ConcurrentDictionary<long, CommandContext>();
        /// <summary>
        /// additional message handlers
        /// </summary>
        protected readonly Dictionary<EventKey, Func<UnifiedMessage, Task>> Handlers =
            new Dictionary<EventKey, Func<UnifiedMessage, Task>>();
        /// <summary>
        /// Initializes a new instance of the <see cref="UnifiedController"/> class.
        /// </summary>
        /// <param name="setup">controller setup envelope</param>
        /// <param name="executor">The command executor.</param>
        protected UnifiedController(ControllerSetup setup, IExecutor executor)
        {
            _setup = setup;
            Executor = executor;
        }
        /// <summary>
        /// handle POST request
        /// </summary>
        /// <remarks>
        /// a common case is to create a controller, create a POST method handler, and set the address of this method as webhook address
        /// </remarks>
        /// <param name="request">request body</param>
        /// <returns></returns>
        protected async Task<IActionResult> PostHandler(JObject request)
        {
            await Task.Run(() => _setup.ParseRequest(request)).ContinueWith(maybe =>
            {
                var result = maybe.Result;
                if (result.HasValue)
                    Task.WaitAll(
                        QuickAnswer(result.Value),
                        ProcessRequest(result.Value));
                else ParseFailedHandler(result.Exception);
            });

            return _setup.RequestResult();
        }
        /// <summary>
        /// handle case of failed request parsing
        /// </summary>
        /// <param name="resultException">parse exception</param>
        protected abstract void ParseFailedHandler(Exception resultException);
        /// <summary>
        /// Processes the request.
        /// </summary>
        /// <param name="message">The message.</param>
        private async Task ProcessRequest(UnifiedMessage message)
        {
            try
            {
                if (await ProcessSpecialMessages(message)) return;

                if (await CheckMessageProcessed(message)) return;

                PreProcessMessage(message);

                if (message.Key.IsSimple())
                    await GeneralHandler(message);
                else
                {
                    if (Handlers.TryGetValue(message.Key, out var handler))
                        await handler(message);
                    else
                        await FallbackHandler(message);
                }
            }
            catch (Exception ex) { LogException(ex); }
            finally { _isInit = true; }

        }
        /// <summary>
        /// handle complex message in case specific handler was not found
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        protected abstract Task FallbackHandler(UnifiedMessage message);
        /// <summary>
        /// quick answer delegate (for messages like 'your query is being processed' until bot warms up)
        /// </summary>
        /// <param name="message">The message.</param>
        private async Task QuickAnswer(UnifiedMessage message)
        {
            if (!_isInit) await QuickAnswerHandler(message);
        }
        /// <summary>
        /// quick answer handler (for messages like 'your query is being processed' until bot warms up)
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        protected abstract Task QuickAnswerHandler(UnifiedMessage message);
        /// <summary>
        /// general message handler (for ones with key.IsSimple() == true)
        /// </summary>
        /// <param name="message">The message.</param>
        private async Task GeneralHandler(UnifiedMessage message)
        {
            if (_setup.DropContextOnNewCommand && IsCommand(PrepareCommandName(message.Text)))
            {
                Contexts.TryRemove(message.Peer.Id, out _);
                await ProcessCommand(message);
                return;
            }

            if (Contexts.TryGetValue(message.Peer.Id, out var ctx))
            {
                if (ctx.IsProcessed)
                    Contexts.TryRemove(message.Peer.Id, out _);
                else
                {
                    var options = ctx.Get(false);
                    if (_setup.ValidateInlineConformance && options.Any())
                    {
                        if (!options.Contains(message.Text))
                        {
                            await ProcessInlineMismatch(message);
                            return;
                        }
                        ctx.Set(false);
                    }

                    ctx.PutAttachments(message.Attachments);

                    if (!string.IsNullOrWhiteSpace(message.Text))
                        ctx.Add(true, message.Text);
                    if (ctx.Count() == Executor.GetArgsCount(ctx))
                        Contexts.TryRemove(message.Peer.Id, out _);
                    await ExecuteAndRespond(ctx, message.Peer);
                    return;
                }
            }

            if (!string.IsNullOrWhiteSpace(message.Text)) await ProcessCommand(message);
        }
        /// <summary>
        /// process command enclosed in message
        /// </summary>
        /// <param name="message">The message.</param>
        private async Task ProcessCommand(UnifiedMessage message)
        {
            var args = message.Text.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);
            if (args.Any())
            {
                var cmd = PrepareCommandName(args[0].ToLower());
                if (IsCommand(cmd))
                {
                    var ctx = new CommandContext(cmd);
                    ctx.PutAttachments(message.Attachments).Add(args.Skip(1));
                    if (ctx.Count() < Executor.GetArgsCount(ctx))
                        Contexts[message.Peer.Id] = ctx;
                    await ExecuteAndRespond(ctx, message.Peer);
                }
                else await ProcessPlainMessage(message);
            }
        }
        /// <summary>
        /// execute command given in context and send resulting message via <see cref="Executor{T}.MessageCreated"/>
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="chat">The chat.</param>
        protected virtual async Task ExecuteAndRespond(CommandContext context, ChatInfo chat)
        {
            var message = await Executor.Execute(context, chat);
            Executor.InvokeMessageCreated(message);
        }
        /// <summary>
        /// process string with, possibly, command name - trim special symbols, etc
        /// </summary>
        /// <param name="s">The string.</param>
        /// <returns></returns>
        protected virtual string PrepareCommandName(string s)
        {
            return s;
        }
        /// <summary>
        /// Preprocess the message.
        /// </summary>
        /// <param name="message">The message.</param>
        protected virtual void PreProcessMessage(UnifiedMessage message) { }
        /// <summary>
        /// Determines whether the specified text is command.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns>
        ///   <c>true</c> if the specified text is command; otherwise, <c>false</c>.
        /// </returns>
        protected virtual bool IsCommand(string text) => Executor.CommandExists(text);
        /// <summary>
        /// Checks if the message was already processed.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns>true if the message was processed</returns>
        protected virtual Task<bool> CheckMessageProcessed(UnifiedMessage message) => Task.FromResult(!ProcessedMessages.Add(message.MessageId));
        /// <summary>
        /// Processes the special messages (like 'stop', 'debug' etc) 
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        protected virtual Task<bool> ProcessSpecialMessages(UnifiedMessage message)
        {
            return Task.FromResult(false);
        }
        /// <summary>
        /// Processes the case message text doesn't match one of present in context's options
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        protected abstract Task ProcessInlineMismatch(UnifiedMessage message);
        /// <summary>
        /// Processes the plain message (not command, not in context)
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        protected abstract Task ProcessPlainMessage(UnifiedMessage message);
        /// <summary>
        /// Logs the exception.
        /// </summary>
        /// <param name="ex">The ex.</param>
        protected abstract void LogException(Exception ex);
        /// <summary>
        /// Tries to get command context.
        /// </summary>
        /// <param name="peerId">The peer identifier.</param>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        protected static bool TryGetContext(long peerId, out CommandContext context) => Contexts.TryGetValue(peerId, out context);
        /// <summary>
        /// Tries the remove context.
        /// </summary>
        /// <param name="peerId">The peer identifier.</param>
        /// <param name="ctx">The removed context.</param>
        /// <returns></returns>
        protected static bool TryRemoveContext(long peerId, out CommandContext ctx) =>
                    Contexts.TryRemove(peerId, out ctx);
    }
}
