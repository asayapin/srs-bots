﻿namespace srs.Bots.Data
{
    /// <summary>
    /// chat info container
    /// </summary>
    public class ChatInfo
    {
        /// <summary>
        /// Gets or sets user identifier.
        /// </summary>
        /// <value>
        /// User identifier.
        /// </value>
        public long Id { get; set; }
        /// <summary>
        /// Gets or sets user name.
        /// </summary>
        /// <value>
        /// User name.
        /// </value>
        public string Name { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether chat is personal.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is personal; otherwise (group chat), <c>false</c>.
        /// </value>
        public bool IsPersonal { get; set; }
        /// <summary>
        /// helper constructor for id wrapping
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public static ChatInfo FromId(long id) => new ChatInfo{Id = id};
    }
}
