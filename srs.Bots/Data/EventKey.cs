﻿using System.Linq;

namespace srs.Bots.Data
{
    /// <summary>
    /// event key for handlers
    /// </summary>
    public class EventKey
    {
        /// <summary>
        /// Determines whether the specified <see cref="System.Object" />, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public sealed override bool Equals(object obj)
        {
            return obj?.GetType() == GetType() 
                   && GetType()
                       .GetProperties()
                       .Select(x => x.GetValue(obj)?.Equals(x.GetValue(this)) == true)
                       .All(x => x);
        }
        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public sealed override int GetHashCode()
        {
            return GetType().GetProperties().Select(x => x.GetValue(this)?.GetHashCode() ?? 0)
                .Aggregate(601067, (acc, val) => unchecked(acc * 441829 + val));
        }
        /// <summary>
        /// Determines whether this instance is simple (can be handled with general handler)
        /// </summary>
        /// <returns>
        ///   <c>true</c> if this instance is simple; otherwise, <c>false</c>.
        /// </returns>
        public virtual bool IsSimple() => true;
    }
}
