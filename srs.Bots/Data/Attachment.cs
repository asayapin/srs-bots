﻿namespace srs.Bots.Data
{
    /// <summary>
    /// attachment wrapper
    /// </summary>
    public class Attachment
    {
        /// <summary>
        /// create attachment
        /// </summary>
        /// <param name="caption">attachment caption</param>
        /// <param name="data">arbitrary data</param>
        public Attachment(string caption, object data)
        {
            Caption = caption;
            Data = data;
        }
        /// <summary>
        /// attachment caption
        /// </summary>
        public string Caption { get; }
        /// <summary>
        /// attachment data
        /// </summary>
        public object Data { get; }
    }
}
