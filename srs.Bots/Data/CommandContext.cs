﻿using System.Collections.Concurrent;
using System.Collections.Generic;

namespace srs.Bots.Data
{
    /// <summary>
    /// command context - envelope for recent history of interacting with user
    /// </summary>
    public class CommandContext
    {
        /// <summary>
        /// Gets the command invoked by user
        /// </summary>
        /// <value>
        /// The command.
        /// </value>
        public string Command { get; private set; }
        private readonly List<string> _arguments = new List<string>();
        private readonly List<string> _options = new List<string>();
        private readonly ConcurrentQueue<Attachment> _attachments = new ConcurrentQueue<Attachment>();
        /// <summary>
        /// Gets a value indicating whether the command is processed.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is processed; otherwise, <c>false</c>.
        /// </value>
        public bool IsProcessed { get; private set; }
        /// <summary>
        /// Set the context as processed.
        /// </summary>
        public void SetProcessed() => IsProcessed = true;
        /// <summary>
        /// Initializes a new instance of the <see cref="CommandContext"/> class.
        /// </summary>
        /// <param name="command">The command.</param>
        public CommandContext(string command)
        {
            Command = command;
        }
        /// <summary>
        /// update context to act like another command
        /// </summary>
        /// <param name="command">The new command.</param>
        /// <param name="clearArguments">if set to <c>true</c> [clear arguments].</param>
        /// <param name="clearAttachments">if set to <c>true</c> [clear attachments].</param>
        /// <param name="clearOptions">if set to <c>true</c> [clear options].</param>
        /// <returns></returns>
        public CommandContext ContinueAs(string command, bool clearArguments = true, bool clearAttachments = false, bool clearOptions = false)
        {
            Command = command;
            if (clearArguments) _arguments.Clear();
            if (clearAttachments) _attachments.Clear();
            if (clearOptions) _options.Clear();
            return this;
        }
        /// <summary>
        /// add attachment to internal attachments queue
        /// </summary>
        /// <param name="caption">The caption.</param>
        /// <param name="data">The data.</param>
        public void PushAttachment(string caption, object data) => _attachments.Enqueue(new Attachment(caption, data));
        /// <summary>
        /// add attachment to internal attachments queue
        /// </summary>
        /// <param name="attachment">The attachment.</param>
        public void PushAttachment(Attachment attachment) => _attachments.Enqueue(attachment);
        /// <summary>
        /// try to pop attachment from internal queue
        /// </summary>
        /// <param name="attachment">The attachment.</param>
        /// <returns></returns>
        public bool TryPopAttachment(out Attachment attachment) => _attachments.TryDequeue(out attachment);

        /// <summary>
        /// get index-th item of arguments/options list
        /// </summary>
        /// <param name="index">0-based, wrapping index; negative values takes from end (-1 == last, -2 == pre-last, etc)</param>
        /// <param name="arg">true to get from arguments, false to get from inline options</param>
        /// <returns></returns>
        public string this[int index, bool arg = true]
        {
            get
            {
                var target = arg ? _arguments : _options;
                if (target.Count == 0) return null;
                var ix = index % target.Count;
                if (ix < 0) ix += target.Count;
                return target[ix];
            }
        }
        /// <summary>
        /// add strings to either arguments or options
        /// </summary>
        /// <param name="s"></param>
        /// <param name="arg">true to get from arguments, false to get from inline options</param>
        public void Add(IEnumerable<string> s, bool arg = true)
        {
            var target = arg ? _arguments : _options;
            target.AddRange(s);
        }
        /// <summary>
        /// add strings to either arguments or options
        /// </summary>
        /// <param name="s"></param>
        /// <param name="arg">true to get from arguments, false to get from inline options</param>
        public void Add(bool arg = true, params string[] s)
        {
            var target = arg ? _arguments : _options;
            target.AddRange(s);
        }
        /// <summary>
        /// clear and add strings to either arguments or options
        /// </summary>
        /// <param name="s"></param>
        /// <param name="arg">true to set arguments, false to set inline options</param>
        public void Set(bool arg = true, params string[] s)
        {
            var target = arg ? _arguments : _options;
            target.Clear();
            target.AddRange(s);
        }
        /// <summary>
        /// clear and add strings to either arguments or options
        /// </summary>
        /// <param name="s"></param>
        /// <param name="arg">true to set arguments, false to set inline options</param>
        public void Set(IEnumerable<string> s, bool arg = true)
        {
            var target = arg ? _arguments : _options;
            target.Clear();
            target.AddRange(s);
        }
        /// <summary>
        /// get arguments or options
        /// </summary>
        /// <param name="arg">true = arguments, options otherwise</param>
        /// <returns></returns>
        public string[] Get(bool arg = true) => (arg ? _arguments : _options).ToArray();
        /// <summary>
        /// get count of inner storage
        /// </summary>
        /// <param name="args">if set to <c>true</c> [arguments], otherwise [options].</param>
        /// <returns></returns>
        public int Count(bool args = true)
        {
            return (args ? _arguments : _options).Count;
        }
    }
}
