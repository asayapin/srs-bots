﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace srs.Bots.Data
{
    /// <summary>
    /// message to be sent to client
    /// </summary>
    public class PeerMessage
    {
        /// <summary>
        /// peer id
        /// </summary>
        public readonly long Recipient;
        /// <summary>
        /// message text (or document body)
        /// </summary>
        public readonly string Message;
        /// <summary>
        /// document caption
        /// </summary>
        public readonly string Caption;
        /// <summary>
        /// is message wrapping file
        /// </summary>
        public readonly bool IsFile;
        /// <summary>
        /// inline/keyboard options
        /// </summary>
        private readonly List<string> _options = new List<string>();
        /// <summary>
        /// inline/keyboard options
        /// </summary>
        public string[] Options => _options.ToArray();

        /// <summary>
        /// buttons layout per row
        /// </summary>
        public int[] ButtonsInRow
        {
            get { return _buttonsInRow ?? Enumerable.Range(0, 10).Select(x => 1).ToArray(); }
            set { _buttonsInRow = value; }
        }

        /// <summary>
        /// create document message
        /// </summary>
        /// <param name="caption">document caption</param>
        /// <param name="message">document body</param>
        /// <param name="recipient">peer id</param>
        protected PeerMessage(string caption, string message, long recipient)
        {
            IsFile = true;
            Caption = caption;
            Message = message;
            Recipient = recipient;
        }
        /// <summary>
        /// create text message
        /// </summary>
        /// <param name="message">text</param>
        /// <param name="recipient">peer id</param>
        protected PeerMessage(string message, long recipient)
        {
            Message = message;
            Recipient = recipient;
        }
        /// <summary>
        /// create text message
        /// </summary>
        /// <param name="s">text</param>
        /// <param name="chat">peer chat info</param>
        /// <returns>new message</returns>
        public static PeerMessage FromText(string s, ChatInfo chat) => new PeerMessage(s, chat.Id);
        /// <summary>
        /// create document message
        /// </summary>
        /// <param name="caption">document caption</param>
        /// <param name="body">document content</param>
        /// <param name="chat">peer chat info</param>
        /// <returns>new message</returns>
        public static PeerMessage AsFile(string caption, string body, ChatInfo chat) => new PeerMessage(caption, body, chat.Id);
        /// <summary>
        /// wrap this instance in task
        /// </summary>
        /// <returns></returns>
        public Task<PeerMessage> AsTask() => new ValueTask<PeerMessage>(this).AsTask();
        /// <summary>
        /// add inline/keyboard options
        /// </summary>
        /// <param name="s">options</param>
        /// <param name="ctx">context to fill</param>
        /// <returns>the message passed in</returns>
        public PeerMessage WithOptions(IEnumerable<string> s, CommandContext ctx)
        {
            if (s is null) return this;
            var array = s as string[] ?? s.ToArray();
            _options.AddRange(array);
            ctx?.Set(_options, false);
            return this;
        }
        /// <summary>
        /// buttons layout per row
        /// </summary>
        private int[] _buttonsInRow;
        /// <summary>
        /// Sets the keyboard layout.
        /// </summary>
        /// <param name="buttonsInRow">The buttons in row.</param>
        /// <returns></returns>
        public PeerMessage SetKeyboardSchema(params int[] buttonsInRow)
        {
            var ints = buttonsInRow.Where(x => x > 0 && x < 5).Take(10).ToList();
            while (ints.Count < 10) ints.Add(ints.Last());
            while (ints.Sum() > 40) ints.RemoveAt(ints.Count - 1);
            ButtonsInRow = ints.ToArray();
            return this;
        }
    }
}
