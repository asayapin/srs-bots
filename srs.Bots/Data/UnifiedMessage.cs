﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace srs.Bots.Data
{
    /// <summary>
    /// general message, received from external service
    /// </summary>
    public class UnifiedMessage
    {
        /// <summary>
        /// Gets or sets the message identifier.
        /// </summary>
        /// <value>
        /// The message identifier.
        /// </value>
        public long MessageId { get; set; }
        /// <summary>
        /// Gets or sets the key.
        /// </summary>
        /// <value>
        /// The key.
        /// </value>
        public EventKey Key { get; set; }
        /// <summary>
        /// Gets or sets the peer.
        /// </summary>
        /// <value>
        /// The peer.
        /// </value>
        public ChatInfo Peer { get; set; }
        /// <summary>
        /// Gets or sets the text.
        /// </summary>
        /// <value>
        /// The text.
        /// </value>
        public string Text { get; set; }
        /// <summary>
        /// Gets or sets the attachments.
        /// </summary>
        /// <value>
        /// The attachments.
        /// </value>
        public Attachment[] Attachments { get; set; }
        /// <summary>
        /// Gets or sets the original request.
        /// </summary>
        /// <value>
        /// The original request.
        /// </value>
        public JObject OriginalRequest { get; set; }
        /// <summary>
        /// Gets or sets the timestamp.
        /// </summary>
        /// <value>
        /// The timestamp.
        /// </value>
        public DateTime Timestamp { get; set; }

        private Dictionary<string, object> _data = new Dictionary<string, object>();
        /// <summary>
        /// load key-value data to inner storage
        /// </summary>
        /// <param name="data">The data.</param>
        public void LoadData(IEnumerable<KeyValuePair<string, object>> data)
        {
            _data = new Dictionary<string, object>(data);
        }
        /// <summary>
        /// get item from inner storage
        /// </summary>
        /// <typeparam name="T">type to convert value to</typeparam>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public T Get<T>(string key) => _data.TryGetValue(key, out var o) && o is T t ? t : default;
    }
}
