﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using srs.Bots.Attributes;
using srs.Bots.Data;

namespace srs.Bots.CommandAbstractions
{
    /// <summary>
    /// general command executor
    /// </summary>
    public class Executor<T> : IExecutor where T : CommandDependencies
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Executor{T}"/> class.
        /// </summary>
        /// <param name="dependencies">The dependencies.</param>
        /// <param name="assembly">The assembly.</param>
        public Executor(T dependencies, Assembly assembly)
        {
            dependencies.Executor = this;

            LoadCommands(assembly, dependencies);
        }

        /// <summary>
        /// The commands able to be processed
        /// </summary>
        protected static readonly ConcurrentDictionary<string, Command<T>> Commands = new ConcurrentDictionary<string, Command<T>>();
        /// <summary>
        /// Occurs when [message created].
        /// </summary>
        public event Action<PeerMessage> MessageCreated;
        /// <summary>
        /// Invokes the message created event.
        /// </summary>
        /// <param name="message">The message.</param>
        public void InvokeMessageCreated(PeerMessage message) => MessageCreated?.Invoke(message);

        /// <summary>
        /// check whether command is registered
        /// </summary>
        /// <param name="name">command name</param>
        /// <returns></returns>
        public bool CommandExists(string name) => Commands.ContainsKey(name);

        /// <summary>
        /// Loads commands from specified assembly
        /// </summary>
        /// <param name="assembly">The assembly, in general case - pass the assembly where all your commands are defined</param>
        /// <param name="dep">The dependencies envelope.</param>
        internal void LoadCommands(Assembly assembly, T dep)
        {
            foreach (var type in assembly.GetTypes().Where(x => typeof(Command<T>).IsAssignableFrom(x)))
            {
                try
                {
                    if (!(Activator.CreateInstance(type) is Command<T> command)) continue;
                    var attr = type.GetCustomAttribute<CommandAttribute>();
                    if (attr is null) continue;
                    var cmd = command.SetArgumentsCount(attr);
                    cmd.Dependencies = dep;
                    Commands[attr.MainName] = cmd;
                    var names = type.GetCustomAttribute<PseudonymsAttribute>();
                    if (names is null) continue;
                    foreach (var name in names.Names)
                    {
                        Commands[name] = cmd;
                    }
                }
                catch { /* ignored*/ }
            }
        }
        /// <summary>
        /// Gets the max arguments count.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        public int? GetArgsCount(CommandContext context)
        {
            return Commands.TryGetValue(context.Command, out var cmd) ? cmd.ArgCount : 0;
        }
        /// <summary>
        /// execute command based on context
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="chat">The chat to respond to.</param>
        /// <returns></returns>
        public async Task<PeerMessage> Execute(CommandContext context, ChatInfo chat)
        {
            if (Commands.TryGetValue(context.Command, out var cmd))
                return await cmd.ProcessCommand(context, chat);

            return PeerMessage.FromText($"'{context.Command}' is not a valid command", chat);
        }
    }
}
