﻿using System;
using System.Threading.Tasks;
using srs.Bots.Data;

namespace srs.Bots.CommandAbstractions
{
    /// <summary>
    /// command executor interface
    /// </summary>
    public interface IExecutor
    {
        /// <summary>
        /// Occurs when [message created].
        /// </summary>
        event Action<PeerMessage> MessageCreated;

        /// <summary>
        /// Invokes the message created event.
        /// </summary>
        /// <param name="message">The message.</param>
        void InvokeMessageCreated(PeerMessage message);
        /// <summary>
        /// check whether command is registered
        /// </summary>
        /// <param name="name">command to check</param>
        /// <returns></returns>
        bool CommandExists(string name);

        /// <summary>
        /// Gets the max arguments count.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <returns></returns>
        int? GetArgsCount(CommandContext context);

        /// <summary>
        /// execute command based on context
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="chat">The chat to respond to.</param>
        /// <returns></returns>
        Task<PeerMessage> Execute(CommandContext context, ChatInfo chat);
    }
}