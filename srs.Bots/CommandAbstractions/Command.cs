﻿using System.Threading.Tasks;
using srs.Bots.Attributes;
using srs.Bots.Data;

namespace srs.Bots.CommandAbstractions
{
    /// <summary>
    /// command class - needs to be inherited from in order to allow bot to process commands
    /// </summary>
    public abstract class Command<T> where T : CommandDependencies
    {
        /// <summary>
        /// The command dependencies
        /// </summary>
        public T Dependencies { get; internal set; }
        /// <summary>
        /// Gets the argument count.
        /// </summary>
        /// <value>
        /// The argument count; null means that command itself controls when it's processed
        /// </value>
        public int? ArgCount { get; private set; }
        /// <summary>
        /// handles user input, split by command name and arguments
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="chat">The chat to respond to.</param>
        /// <returns>message to respond with</returns>
        public abstract Task<PeerMessage> ProcessCommand(CommandContext context, ChatInfo chat);

        /// <summary>
        /// Sets the arguments count from attribute; called by <see cref="Executor{T}.LoadCommands"/>
        /// </summary>
        /// <param name="attr">The attribute.</param>
        /// <returns></returns>
        public Command<T> SetArgumentsCount(CommandAttribute attr)
        {
            ArgCount = attr.Arguments;
            return this;
        }

        /// <summary>
        /// clear user-bound command state
        /// </summary>
        /// <param name="chat">chat info to clear state for</param>
        /// <returns>success state</returns>
        public virtual bool ClearState(ChatInfo chat) => true;
    }
}
