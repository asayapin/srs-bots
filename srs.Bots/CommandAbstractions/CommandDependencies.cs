﻿namespace srs.Bots.CommandAbstractions
{
    /// <summary>
    /// general dependencies envelope
    /// </summary>
    public abstract class CommandDependencies
    {
        /// <summary>
        /// Gets the executor.
        /// </summary>
        /// <value>
        /// The executor.
        /// </value>
        public IExecutor Executor { get; internal set; }
    }
}
