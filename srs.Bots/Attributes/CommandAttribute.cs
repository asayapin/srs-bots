﻿using System;

namespace srs.Bots.Attributes
{
    /// <summary>
    /// mark the command and pass some metadata
    /// </summary>
    /// <seealso cref="System.Attribute" />
    [AttributeUsage(AttributeTargets.Class)]
    public class CommandAttribute : Attribute
    {
        /// <summary>
        /// Gets the main name of command.
        /// </summary>
        public string MainName { get; }

        /// <summary>
        /// Gets the arguments.
        /// </summary>
        /// <value>
        /// The arguments.
        /// </value>
        public int Arguments { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="CommandAttribute"/> class.
        /// </summary>
        /// <param name="mainName">main command name; additional names can be added with <see cref="PseudonymsAttribute"/></param>
        /// <param name="arguments">The arguments count; set to -1 to control processed state by command instead of controller.</param>
        public CommandAttribute(string mainName, int arguments = 0)
        {
            MainName = mainName;
            Arguments = arguments == -1 ? int.MaxValue : arguments;
        }
    }
}
