﻿using System;
using System.Linq;

namespace srs.Bots.Attributes
{
    /// <summary>
    /// pseudonyms container
    /// </summary>
    /// <seealso cref="System.Attribute" />
    [AttributeUsage(AttributeTargets.Class)]
    public class PseudonymsAttribute : Attribute
    {
        /// <summary>
        /// Gets the command names.
        /// </summary>
        /// <value>
        /// The command names.
        /// </value>
        public string[] Names { get; }
        /// <summary>
        /// Initializes a new instance of the <see cref="PseudonymsAttribute"/> class.
        /// </summary>
        /// <param name="names">Command names.</param>
        public PseudonymsAttribute(params string[] names)
        {
            Names = names.ToArray();
        }
    }
}