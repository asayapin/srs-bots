﻿using Microsoft.Extensions.DependencyInjection;
using srs.Bots.Utility;

namespace srs.Bots.DIAbstractions
{
    /// <summary>
    /// extensions for dependency injection
    /// </summary>
    public static class DIExtensions
    {
        /// <summary>
        /// start injection setup
        /// </summary>
        /// <param name="services">The services.</param>
        /// <param name="injectionType">Type of the injection.</param>
        /// <returns></returns>
        public static ServiceInstaller UseBotsDI(this IServiceCollection services, ServiceInjectionType injectionType = ServiceInjectionType.Singleton)
        {
            return new ServiceInstaller(services, injectionType);
        }
    }
}