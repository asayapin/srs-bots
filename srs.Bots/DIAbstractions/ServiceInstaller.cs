﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Linq;
using srs.Bots.CommandAbstractions;
using srs.Bots.ControllerAbstractions;
using srs.Bots.Data;
using srs.Bots.Utility;

namespace srs.Bots.DIAbstractions
{
    /// <summary>
    /// general service installer
    /// </summary>
    public class ServiceInstaller
    {
        private readonly IServiceCollection _services;
        private readonly ServiceInjectionType _injectionType;
        private readonly ControllerSetup _setup;

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceInstaller"/> class.
        /// </summary>
        /// <param name="services">di container</param>
        /// <param name="injectionType">Type of the injection.</param>
        internal ServiceInstaller(IServiceCollection services, ServiceInjectionType injectionType)
        {
            _services = services;
            _injectionType = injectionType;
            _setup = new ControllerSetup();
        }
        /// <summary>
        /// specify general post request result factory
        /// </summary>
        /// <param name="factory">result factory</param>
        /// <returns></returns>
        public ServiceInstaller RespondWith(Func<IActionResult> factory)
        {
            _setup.RequestResult = factory;
            return this;
        }
        /// <summary>
        /// specify parser delegate to convert incoming json to unified message format
        /// </summary>
        /// <param name="parser">parser delegate</param>
        /// <returns></returns>
        public ServiceInstaller ParseRequestWith(Func<JObject, Maybe<UnifiedMessage>> parser)
        {
            _setup.ParseRequest = parser;
            return this;
        }
        /// <summary>
        /// specify behavior of command received while another command is running
        /// </summary>
        /// <param name="dropPrevious">true - drop previous, otherwise process message as additional argument</param>
        /// <returns></returns>
        public ServiceInstaller OnNewCommand(bool dropPrevious)
        {
            _setup.DropContextOnNewCommand = dropPrevious;
            return this;
        }
        /// <summary>
        /// specify if message should be validated in case of inlines present
        /// </summary>
        /// <param name="validate">true - message is checked to be present in context's options, if present - process, otherwise call <see cref="UnifiedController"/>.ProcessInlineMismatch; false - skip check</param>
        /// <returns></returns>
        public ServiceInstaller ValidateInline(bool validate)
        {
            _setup.ValidateInlineConformance = validate;
            return this;
        }
        /// <summary>
        /// register type to get assembly for commands discovery
        /// </summary>
        /// <param name="t">type to get assembly; common case is typeof(SomeCustomCommand)</param>
        /// <returns></returns>
        public ServiceInstaller UsingCommandsAndDependenciesFrom<T>(Type t) where T : CommandDependencies
        {
            _services.AddSingleton<T>();
            _services.AddSingleton<IExecutor>(isp => new Executor<T>(isp.GetService<T>(), t.Assembly));
            return this;
        }
        /// <summary>
        /// Adds to <see cref="IServiceCollection"/>.
        /// </summary>
        /// <param name="services">The collection.</param>
        /// <exception cref="TypeInitializationException">controller dependencies are not fulfilled</exception>
        public void AddTo(IServiceCollection services)
        {
            if (!_setup.IsFilled())
                throw new TypeInitializationException(nameof(ControllerSetup), new Exception("controller dependencies are not fulfilled"));
            switch (_injectionType)
            {
                case ServiceInjectionType.Scoped:
                    services.AddScoped(isp => _setup);
                    break;
                case ServiceInjectionType.Transient:
                    services.AddTransient(isp => _setup);
                    break;
                default:
                    services.AddSingleton(isp => _setup);
                    break;
            }
        }
    }
}
