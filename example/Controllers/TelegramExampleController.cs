﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using srs.Bots.CommandAbstractions;
using srs.Bots.ControllerAbstractions;
using srs.Bots.Data;
using srs.Bots.tlg;

namespace example.Controllers
{
    [ApiController]
    [Route("api/tlg")]
    public class TelegramExampleController : TelegramControllerBase
    {
        /// <summary>
        /// use this to send debug info, like exceptions
        /// </summary>
        private readonly long _adminId;

        static TelegramExampleController()
        {
            // this should be set prior to instance ctor invocation; it will be used to obtain bot access token from config
            BotTokenKey = "tlg:botToken";
            // these two can be set in instance ctor
            InlineMismatchFormat = "{0} is not a valid option";
            QuickAnswer = "command is being processed, please wait";
        }
        [HttpPost]
        public async Task<IActionResult> Post(JObject request)
        {
            return await PostHandler(request);
        }

        public TelegramExampleController(ControllerSetup setup, IExecutor executor, IConfiguration config) : base(setup, executor, config)
        {
            _adminId = config.GetValue<long>("tlg:botAdminId");
        }

        protected override Task FallbackHandler(UnifiedMessage message)
        {
            Executor.InvokeMessageCreated(PeerMessage.FromText($"failed to process message: {message.OriginalRequest}", message.Peer));
            return Task.CompletedTask;
        }

        protected override void LogException(Exception ex)
        {
            Executor.InvokeMessageCreated(PeerMessage.FromText($"exception encountered: {ex}", ChatInfo.FromId(_adminId)));
        }
    }
}
