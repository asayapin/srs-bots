﻿using System.Threading.Tasks;
using srs.Bots.Attributes;
using srs.Bots.CommandAbstractions;
using srs.Bots.Data;

namespace example.Controllers
{
    [Command("hi", 1)]
    [Pseudonyms("hello", "greet", "hola", "привет")]
    public class GreetCommand : Command<BotDependencies>
    {
        public override Task<PeerMessage> ProcessCommand(CommandContext context, ChatInfo chat)
        {
            if (context.Count() == 0)
                return PeerMessage.FromText("What's your name?", chat).AsTask();
            return PeerMessage.FromText($"Hi, {context[-1]}", chat).AsTask();
        }
    }
}
