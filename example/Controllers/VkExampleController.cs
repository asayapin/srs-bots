﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using srs.Bots.CommandAbstractions;
using srs.Bots.ControllerAbstractions;
using srs.Bots.Data;
using srs.Bots.vk;
using VkNet.Abstractions;

namespace example.Controllers
{
    [ApiController]
    [Route("api/vk")]
    public class VkExampleController : VkControllerBase
    {
        private readonly long _adminId;

        static VkExampleController()
        {
            // this should be set prior to instance ctor invocation; it will be used to obtain bot access token from config
            GroupIdKey = "vk:botGroupId";
            // these two can be set in instance ctor
            InlineMismatchFormat = "{0} is not a valid option";
            QuickAnswer = "command is being processed, please wait";
        }

        public VkExampleController(ControllerSetup setup, IExecutor executor, IConfiguration config, IVkApi vkApi) : base(setup, executor, config, vkApi)
        {
            _adminId = config.GetValue<long>("vk:botAdminId");
        }
        [HttpPost]
        public async Task<IActionResult> Post(JObject request)
        {
            return await PostHandler(request);
        }

        protected override Task FallbackHandler(UnifiedMessage message)
        {
            Executor.InvokeMessageCreated(PeerMessage.FromText($"failed to process message: {message.OriginalRequest}", message.Peer));
            return Task.CompletedTask;
        }

        protected override void LogException(Exception ex)
        {
            Executor.InvokeMessageCreated(PeerMessage.FromText($"exception encountered: {ex}", ChatInfo.FromId(_adminId)));
        }

        protected override Task ProcessWallPostReply(UnifiedMessage message)
        {
            Executor.InvokeMessageCreated(PeerMessage.FromText($"user https://vk.com/id{message.Peer.Id} has posted on wall: {message.Text}", ChatInfo.FromId(_adminId)));
            return Task.CompletedTask;
        }
    }
}