﻿using System;
using System.Linq;

namespace srs.Bots.AdvancedCommands.Attributes
{
    /// <summary>
    /// attribute to decorate tree/graph steps
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class StepAttribute : Attribute
    {
        /// <summary>
        /// step name
        /// </summary>
        public string Name { get; }
        /// <summary>
        /// steps that may lead to this step
        /// </summary>
        public string[] Parents { get; }
        /// <summary>
        /// steps that are led to by this step
        /// </summary>
        public string[] Children { get; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="parents"></param>
        public StepAttribute(string name, params string[] parents)
        {
            Name = name;
            Parents = parents.ToArray();
            Children = new string[0];
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="parentsCount">number of parents steps in following array (taken from 0-th element)</param>
        /// <param name="parentsAndChldren">list of parent steps followed by children steps</param>
        public StepAttribute(string name, int parentsCount, params string[] parentsAndChldren)
        {
            Name = name;
            Parents = parentsAndChldren.Take(parentsCount).ToArray();
            Children = parentsAndChldren.Skip(parentsCount).ToArray();
        }
    }
}
