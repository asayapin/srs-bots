﻿using System.Collections.Generic;
using System.Linq;
using srs.Bots.AdvancedCommands.Attributes;
using srs.Bots.AdvancedCommands.Internal;

namespace srs.Bots.AdvancedCommands.Config
{
    /// <summary>
    /// graph/tree command setup container
    /// </summary>
    public class GraphCommandSetup
    {
        private readonly Dictionary<string, CommandGraphNode> _nodes = new Dictionary<string, CommandGraphNode>();
        /// <summary>
        /// register node (automatically)
        /// </summary>
        /// <param name="attribute"></param>
        /// <param name="handler"></param>
        /// <returns></returns>
        public GraphCommandSetup AddNode(StepAttribute attribute, StepHandler handler)
        {
            var node = _nodes[attribute.Name] = new CommandGraphNode{Handler = handler, StepName = attribute.Name};
            foreach (var key in attribute.Parents.Where(_nodes.ContainsKey)) _nodes[key].AddChild(node);
            foreach (var key in attribute.Children.Where(_nodes.ContainsKey)) _nodes[key].AddTo(node);
            return this;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="rootNode"></param>
        /// <returns></returns>
        public CommandGraphNode GetCommandsGraph(string rootNode)
        {
            return _nodes.TryGetValue(rootNode, out var node) ? node : null;
        }
    }
}
