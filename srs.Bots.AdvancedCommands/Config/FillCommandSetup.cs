﻿using System.Collections.Generic;
using System.Linq;
using srs.Bots.AdvancedCommands.Internal;

namespace srs.Bots.AdvancedCommands.Config
{
    /// <summary>
    /// setup helper for <see cref="FillCommand{T,TFillable}"/>
    /// </summary>
    public class FillCommandSetup
    {
        internal const string
            KeyInputTypePrompt = "_internal_InputTypePrompt",
            KeyFormOption = "_internal_Form",
            KeyDialogOption = "_internal_Dialog",
            InternalPrefix = "_internal_",
            SuccessResponse = "_internal_SuccessResponse",
            DialogPropertyPrompt = "_internal_DialogPropertyPrompt",
            FormHeader = "_internal_FormHeader",
            DialogPropertyValuePrompt = "_internal_DialogPropertyValuePrompt",
            PropertyViewPrefix = "_internal_PropertyViewPrefix",
            DialogFailedToSetValue = "_internal_DialogFailedToSetValue",
            DialogWrongPropertySelected = "_internal_DialogWrongPropertySelected",
            PropertyExamplePrefix = "_internal_PropertyExamplePrefix";

        private readonly OptionsContainer _container = new OptionsContainer();
        /// <summary>
        /// get options dictionary
        /// </summary>
        /// <returns></returns>
        public IReadOnlyDictionary<string, object> GetOptions() => _container.GetContainer();
        /// <summary>
        /// set property-specific strings
        /// </summary>
        /// <param name="strings">strings list (in triplets): property name, property user text, property example value user text</param>
        /// <returns></returns>
        public FillCommandSetup SetStrings(params string[] strings)
        {
            for (var i = 2; i < strings.Length; i += 3)
            {
                var propName = strings[i - 2];
                var propText = strings[i - 1];
                var propValue = strings[i];
                _container
                    .Set($"{PropertyViewPrefix}{propName}", propText)
                    .Set($"{PropertyExamplePrefix}{propName}", propValue);
            }
            return this;
        }
        /// <summary>
        /// set property-specific strings
        /// </summary>
        /// <param name="strings">strings list (in triplets): property name, property user text, property example value user text</param>
        /// <returns></returns>
        public FillCommandSetup SetStrings(IEnumerable<string> strings) => SetStrings(strings.ToArray());
        /// <summary>
        /// set dialog-specific strings
        /// </summary>
        /// <param name="propPrompt">property prompt string (like 'Choose field to fill')</param>
        /// <param name="valuePrompt">value prompt string with 2 args (like 'Enter value for {0} (e.g. {1})')</param>
        /// <param name="errorFailedToSet">error message in case value can't be set</param>
        /// <param name="errorWrongProperty">error message in case property selected is not found in class</param>
        /// <returns></returns>
        public FillCommandSetup SetDialogStrings(string propPrompt, string valuePrompt, string errorFailedToSet, string errorWrongProperty)
        {
            _container
                .Set(DialogPropertyPrompt, propPrompt)
                .Set(DialogPropertyValuePrompt, valuePrompt)
                .Set(DialogFailedToSetValue, errorFailedToSet)
                .Set(DialogWrongPropertySelected, errorWrongProperty);
            return this;
        }
        /// <summary>
        /// setup common strings
        /// </summary>
        /// <param name="success">response message in case object was successfully loaded</param>
        /// <param name="viewPrefix">option prefix for property text (concatenated with property name to get user text: using prefix 'viewProp' and property 'Name', 'viewPropName' should hold user text for name property)</param>
        /// <param name="examplePrefix">option prefix for property example text</param>
        /// <returns></returns>
        public FillCommandSetup SetCommonStrings(string success, string viewPrefix, string examplePrefix)
        {
            _container
                .Set(SuccessResponse, success)
                .Set(PropertyViewPrefix, viewPrefix)
                .Set(PropertyExamplePrefix, examplePrefix);
            return this;
        }
        /// <summary>
        /// set strings for mode prompt
        /// </summary>
        /// <param name="modePrompt">message to ask for input mode</param>
        /// <param name="formOption">inline option for form loading mode</param>
        /// <param name="dialogOption">inline option for dialog mode</param>
        /// <returns></returns>
        public FillCommandSetup SetModeStrings(string modePrompt, string formOption, string dialogOption)
        {
            _container
                .Set(KeyInputTypePrompt, modePrompt)
                .Set(KeyFormOption, formOption)
                .Set(KeyDialogOption, dialogOption);
            return this;
        }
        /// <summary>
        /// set strings specific for form loading
        /// </summary>
        /// <param name="header">form header, sent along with form to load</param>
        /// <returns></returns>
        public FillCommandSetup SetFormStrings(string header)
        {
            _container.Set(FormHeader, header);
            return this;
        }
    }
}
