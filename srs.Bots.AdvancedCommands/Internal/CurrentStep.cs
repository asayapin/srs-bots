﻿namespace srs.Bots.AdvancedCommands.Internal
{
    /// <summary>
    /// current step container for graph/tree commands
    /// </summary>
    internal class CurrentStep
    {
        /// <summary>
        /// current node of navigation graph
        /// </summary>
        public CommandGraphNode StepNode;
        /// <summary>
        /// true if step waits to fill next node using context's last argument
        /// </summary>
        public bool FillFromContext;
    }
}