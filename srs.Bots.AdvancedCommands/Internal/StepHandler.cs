﻿using srs.Bots.Data;

namespace srs.Bots.AdvancedCommands.Internal
{
    /// <summary>
    /// graph node handler delegate
    /// </summary>
    /// <param name="context">context passed to command</param>
    /// <param name="chat">chat of command</param>
    /// <returns>handle result - message + further steps</returns>
    public delegate StepHandleResult StepHandler(CommandContext context, ChatInfo chat);
}