﻿using System;

namespace srs.Bots.AdvancedCommands.Internal
{
    [Flags]
    internal enum NodeTraverseDirection
    {
        Child = 1, Parent = 2, Both
    }
}