﻿using System.Collections.Generic;
using System.Linq;
using srs.Bots.Data;

namespace srs.Bots.AdvancedCommands.Internal
{
    /// <summary>
    /// result of handler invocation
    /// </summary>
    public class StepHandleResult
    {
        /// <summary>
        /// default ctor
        /// </summary>
        /// <param name="result">message to send in response to command</param>
        /// <param name="nextSteps">list of next possible steps</param>
        public StepHandleResult(PeerMessage result, IEnumerable<string> nextSteps)
        {
            Result = result;
            NextSteps = nextSteps.ToArray();
        }
        /// <summary>
        /// message to respond with
        /// </summary>
        public PeerMessage Result { get; }
        /// <summary>
        /// next allowed steps
        /// </summary>
        public string[] NextSteps { get; }
    }
}
