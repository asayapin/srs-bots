﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.Immutable;

namespace srs.Bots.AdvancedCommands.Internal
{
    internal class OptionsContainer
    {
        private readonly ConcurrentDictionary<string, object> _values = new ConcurrentDictionary<string, object>();

        public OptionsContainer Set(string name, object value)
        {
            _values[name] = value;
            return this;
        }

        public IReadOnlyDictionary<string, object> GetContainer() => _values.ToImmutableDictionary();
    }
}