﻿using System.Collections.Generic;
using System.Linq;

namespace srs.Bots.AdvancedCommands.Internal
{
    /// <summary>
    /// node of command graph, holding relationships and handler
    /// </summary>
    public class CommandGraphNode
    {
        internal List<CommandGraphNode> Parents = new List<CommandGraphNode>();
        internal List<CommandGraphNode> Children = new List<CommandGraphNode>();
        internal string StepName;
        internal StepHandler Handler;

        internal void AddChild(CommandGraphNode node)
        {
            if (Children.Contains(node) || Children.Any(x => x.StepName == node.StepName)) return;
            Children.Add(node);
            node.Parents.Add(this);
        }

        internal void AddTo(CommandGraphNode node) => node.AddChild(this);

        internal CommandGraphNode this[string stepName, NodeTraverseDirection direction = NodeTraverseDirection.Child]
        {
            get
            {
                if (!(direction.HasFlag(NodeTraverseDirection.Child) ||
                      direction.HasFlag(NodeTraverseDirection.Parent))) return null;
                var items = new List<CommandGraphNode>();
                if (direction.HasFlag(NodeTraverseDirection.Child)) items.AddRange(Children);
                if (direction.HasFlag(NodeTraverseDirection.Parent)) items.AddRange(Parents);
                return items.FirstOrDefault(x => x.StepName == stepName);
            }
        }

        internal CommandGraphNode Parent => Parents.Count == 1 ? Parents[0] : null;
    }
}