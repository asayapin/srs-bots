﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace srs.Bots.AdvancedCommands.Internal
{
    internal static class ServiceExtensions
    {
        /// <summary>
        /// reflexive set value to property
        /// </summary>
        /// <param name="o">target object</param>
        /// <param name="property">property name</param>
        /// <param name="value">target value</param>
        /// <returns>success state</returns>
        public static bool SetValue(this object o, string property, string value)
        {
            var prop = o?.GetType().GetProperty(property);
            if (prop is null) return false;
            //if (!(prop.GetValue(this) is null)) return false; // TODO : value types... maybe just omit the check?
            try
            {
                var conversionType = prop.PropertyType;
                if (conversionType.IsEnum)
                {
                    prop.SetValue(o, Enum.Parse(conversionType, value));
                    return true;
                }

                if (conversionType.IsGenericType && conversionType.GetGenericTypeDefinition() == typeof(Nullable<>))
                {
                    conversionType = conversionType.GetGenericArguments()[0];
                }
                var val = Convert.ChangeType(value, conversionType);
                prop.SetValue(o, val);
                return true;
            }
            catch { return false; }
        }

        public static string GetKey(this object t, object o = null) => $"{t.GetType().FullName}/{o}";

        public static T Get<T>(this IReadOnlyDictionary<string, object> dict, string name)
        {
            if (dict is { } && dict.TryGetValue(name, out var ret) && ret is T t)
                return t;
            var type = typeof(T);
            if (type.GetConstructors().Any(x => x.GetParameters().Length == 0)) return (T)Activator.CreateInstance(type);
            if (type.IsArray) return (T) Activator.CreateInstance(type, 0);
            if (type == typeof(string)) return (T) (object)string.Empty;
            return default;
        }
    }
}
