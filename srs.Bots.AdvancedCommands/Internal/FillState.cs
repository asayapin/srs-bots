﻿using srs.Bots.AdvancedCommands.Interfaces;

namespace srs.Bots.AdvancedCommands.Internal
{
    internal class FillState<T> where T : IFillable, new()
    {
        public T Value = new T();
        public bool WaitingForValue;
        public FillMode FillMode;
    }
}