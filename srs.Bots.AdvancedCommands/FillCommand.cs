﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using srs.Bots.AdvancedCommands.Config;
using srs.Bots.AdvancedCommands.Interfaces;
using srs.Bots.AdvancedCommands.Internal;
using srs.Bots.CommandAbstractions;
using srs.Bots.Data;
using srs.Bots.Utility;
using static srs.Bots.AdvancedCommands.Config.FillCommandSetup;

namespace srs.Bots.AdvancedCommands
{
    /// <summary>
    /// item filling command
    /// </summary>
    /// <typeparam name="T">dependencies</typeparam>
    /// <typeparam name="TFillable">fillable object</typeparam>
    public abstract class FillCommand<T, TFillable> : Command<T> where T : CommandDependencies where TFillable : IFillable, new()
    {
        private bool _isInit;

        private readonly ConcurrentDictionary<string, FillState<TFillable>> _objects = new ConcurrentDictionary<string, FillState<TFillable>>();
        /// <summary>
        /// instance settings
        /// </summary>
        protected IReadOnlyDictionary<string, object> Settings { get; private set; }
        /// <summary>
        /// load settings to command instance
        /// </summary>
        /// <param name="setup"></param>
        protected void ApplySettings(FillCommandSetup setup)
        {
            var options = setup?.GetOptions();
            Settings = options ?? throw new NullReferenceException();
            _isInit = true;
        }

        /// <inheritdoc cref="Command{T}"/>
        public sealed override Task<PeerMessage> ProcessCommand(CommandContext context, ChatInfo chat)
        {
            if (!_isInit) throw new TypeInitializationException(GetType().FullName, new NullReferenceException("options are not provided"));
            var chatKey = this.GetKey(chat.Id);
            var typeKey = this.GetKey();

            if (!_objects.TryGetValue(chatKey, out var state))
                state = _objects[chatKey] = new FillState<TFillable>();

            TrySetFillMode(context, Settings, state);

            if (state.FillMode == FillMode.None)
            {
                var prompt = Settings.Get<string>(KeyInputTypePrompt);
                var options = new[] {Settings.Get<string>(KeyFormOption), Settings.Get<string>(KeyDialogOption)};
                return PeerMessage.FromText(prompt, chat).WithOptions(options, context).AsTask();
            }

            if (state.Value.IsFilled())
                return PostProcessInstance(chat, state, Settings).AsTask();

            return state.FillMode == FillMode.Dialog 
                ? ProcessDialogInput(context, chat, state, Settings).AsTask() 
                : ProcessFormInput(context, chat, state, Settings).AsTask();
        }

        private PeerMessage ProcessFormInput(CommandContext context, ChatInfo chat, FillState<TFillable> state,
            IReadOnlyDictionary<string, object> settings)
        {
            var text = context[-1]
                .Split(new[] {'\r', '\n'}, StringSplitOptions.RemoveEmptyEntries)
                .Select(x => x.Split(':', 2))
                .ToArray();
            foreach (var pair in text)
            {
                var prop = settings
                    ?.FirstOrDefault(x => x.Value is string s && s == pair[0])
                    .Key.Replace(PropertyViewPrefix, "").Trim();
                var value = pair[1].Trim();
                state.Value.SetValue(prop, value);
            }
            var props = state.Value.GetPropertiesToFill().ToArray();
            if (props.Any())
            {
                var lines = string.Join("\n",
                    props.Select(x =>
                        $"{settings.Get<string>(PropertyViewPrefix + x)}: {settings.Get<string>(PropertyExamplePrefix + x)}"));
                return PeerMessage.FromText(settings.Get<string>(FormHeader) + "\n\n" + lines, chat);
            }
            if (state.Value.IsFilled())
                return PostProcessInstance(chat, state, settings);
            throw new InvalidOperationException("fillable instance not filled but have no props to fill");
        }

        private PeerMessage PostProcessInstance(ChatInfo chat, FillState<TFillable> state, IReadOnlyDictionary<string, object> settings)
        {
            var maybe = ProcessFilledInstance(state.Value);
            if (maybe.HasValue) return maybe.Value;
            ClearState(chat);
            return PeerMessage.FromText(settings.Get<string>(SuccessResponse), chat);
        }

        private static void TrySetFillMode(CommandContext context, IReadOnlyDictionary<string, object> settings, FillState<TFillable> state)
        {
            if (context.Count() != 1) return;
            var selected = context[-1];
            var type = settings.FirstOrDefault(x => x.Value is string s && s == selected).Key
                .Replace(InternalPrefix, "");
            if (!Enum.TryParse(type, out FillMode res))
                context.Set();
            else state.FillMode = res;
        }

        private PeerMessage ProcessDialogInput(CommandContext context, ChatInfo chat, FillState<TFillable> state,
            IReadOnlyDictionary<string, object> settings)
        {
            if (state.WaitingForValue)
            {
                if (!state.Value.SetValue(context[-2], context[-1]))
                    return PeerMessage.FromText(DialogFailedToSetValue, chat);
                state.WaitingForValue = false;
                var props = state.Value.GetPropertiesToFill().ToArray();
                if (props.Any())
                    return PeerMessage.FromText(settings.Get<string>(DialogPropertyPrompt), chat)
                        .WithOptions(
                            props
                                .Select(x => PropertyViewPrefix + x)
                                .Select(settings.Get<string>),
                            context);
                if (state.Value.IsFilled()) return PostProcessInstance(chat, state, settings);
                throw new InvalidOperationException("fillable instance not filled but have no props to fill");
            }
            var prop = context[-1];
            if (!state.Value.GetPropertiesToFill().Contains(prop))
                return PeerMessage.FromText(DialogWrongPropertySelected, chat);
            state.WaitingForValue = true;
            var view = settings.Get<string>(PropertyViewPrefix + prop);
            var example = settings.Get<string>(PropertyExamplePrefix + prop);
            var format = settings.Get<string>(DialogPropertyValuePrompt);
            return PeerMessage.FromText(string.Format(format, view, example), chat);
        }

        /// <inheritdoc cref="Command{T}"/>
        public sealed override bool ClearState(ChatInfo chat) => _objects.TryRemove(this.GetKey(chat.Id), out _);
        /// <summary>
        /// instance postprocessor (for pushing to db, etc)
        /// </summary>
        /// <param name="instance">filled instance</param>
        /// <returns>empty maybe for success, filled maybe (with PeerMessage) in case of failure</returns>
        protected abstract Maybe<PeerMessage> ProcessFilledInstance(TFillable instance);
    }
}
