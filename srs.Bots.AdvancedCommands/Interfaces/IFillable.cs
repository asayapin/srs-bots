﻿using System.Collections.Generic;

namespace srs.Bots.AdvancedCommands.Interfaces
{
    /// <summary>
    /// common interface for object supporting step-by-step filling
    /// </summary>
    public interface IFillable
    {
        /// <summary>
        /// check whether all critical fields are filled
        /// </summary>
        /// <returns></returns>
        bool IsFilled();
        /// <summary>
        /// get all properties names that can be filled (including optional)
        /// </summary>
        /// <returns></returns>
        IEnumerable<string> GetPropertiesToFill();
    }
}
