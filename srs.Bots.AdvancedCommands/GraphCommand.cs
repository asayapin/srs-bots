﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using srs.Bots.AdvancedCommands.Attributes;
using srs.Bots.AdvancedCommands.Config;
using srs.Bots.AdvancedCommands.Internal;
using srs.Bots.CommandAbstractions;
using srs.Bots.Data;
using srs.Bots.Utility;

namespace srs.Bots.AdvancedCommands
{
    /// <summary>
    /// complex command with transition graph
    /// </summary>
    /// <remarks>
    /// requires registering all steps with decorating with <see cref="StepAttribute"/> or manually in constructor
    /// </remarks>
    public abstract class GraphCommand<T> : Command<T> where T : CommandDependencies
    {
        private readonly bool _supportsBackNavigation;
        private readonly NodeTraverseDirection _direction;
        private readonly ConcurrentDictionary<long, CurrentStep> _currentStep = new ConcurrentDictionary<long, CurrentStep>();
        private CommandGraphNode _root;
        private string _prompt;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="supportsBackNavigation">set to true to check for back commands</param>
        /// <param name="supportsBackTraverse"></param>
        protected GraphCommand(bool supportsBackNavigation, bool supportsBackTraverse)
        {
            _supportsBackNavigation = supportsBackNavigation;
            _direction = supportsBackTraverse ? NodeTraverseDirection.Both : NodeTraverseDirection.Child;
        }
        /// <summary>
        /// attribute-based steps discovery
        /// </summary>
        protected GraphCommandSetup DiscoverSteps()
        {
            const BindingFlags flags = BindingFlags.Instance
                                       | BindingFlags.Static
                                       | BindingFlags.Public
                                       | BindingFlags.NonPublic
                                       | BindingFlags.DeclaredOnly;
            var setup = new GraphCommandSetup();
            foreach (var methodInfo in GetType().GetMethods(flags).Where(x => x.GetCustomAttribute<StepAttribute>() is { }))
            {
                var handler =
                    (StepHandler)methodInfo.CreateDelegate(typeof(StepHandler), methodInfo.IsStatic ? null : this);
                var attr = methodInfo.GetCustomAttribute<StepAttribute>();
                setup.AddNode(attr, handler);
            }
            return setup;
        }
        /// <summary>
        /// register root node
        /// </summary>
        /// <param name="setup"></param>
        /// <param name="rootNode"></param>
        protected void SetRoot(GraphCommandSetup setup, string rootNode) => _root = setup.GetCommandsGraph(rootNode);
        /// <summary>
        /// register prompt for case multiple steps are available
        /// </summary>
        /// <param name="prompt"></param>
        protected void SetNextStepPrompt(string prompt) => _prompt = prompt;

        /// <inheritdoc cref="Command{T}"/>
        public sealed override Task<PeerMessage> ProcessCommand(CommandContext context, ChatInfo chat)
        {
            var chatKey = chat.Id;
            var step = _currentStep.ContainsKey(chatKey)
                ? _currentStep[chatKey]
                : _currentStep[chatKey] = new CurrentStep { StepNode = GetRoot() };

            if (_supportsBackNavigation && TryNavigateBack(context[-1], step.StepNode, out var node))
                step.StepNode = node;

            if (step.FillFromContext)
            {
                var target = step.StepNode[context[-1], _direction];
                if (target is null)
                    return RespondTargetNotFound(context[-1], chat);

                step.StepNode = target;
                step.FillFromContext = false;
            }

            var result = step.StepNode.Handler(context, chat);
            var message = result.Result;

            switch (result.NextSteps?.Length)
            {
                case 0:
                case null:
                    break;
                case 1:
                    var target = step.StepNode[result.NextSteps[0], _direction];
                    step.StepNode = target ?? _root;
                    break;
                default:
                    step.FillFromContext = true;
                    message = result.Result.Wrap($"{{0}}\n{(_prompt ?? "Select next step")}", chat)
                        .WithOptions(result.Result.Options.Concat(result.NextSteps), context);
                    break;
            }

            return message.AsTask();
        }
        /// <summary>
        /// check if command tries to navigate back and navigate, if applicable
        /// </summary>
        /// <param name="command">last command</param>
        /// <param name="node">current step node</param>
        /// <param name="target">node to navigate to</param>
        /// <returns>true if navigation can be done</returns>
        protected abstract bool TryNavigateBack(string command, CommandGraphNode node, out CommandGraphNode target);
        /// <summary>
        /// get root command graph node
        /// </summary>
        /// <returns></returns>
        protected CommandGraphNode GetRoot()
        {
            if (_root is { }) return _root;
            throw new TypeInitializationException(GetType().FullName,
                new KeyNotFoundException("root step not found"));
        }
        /// <summary>
        /// create a message upon target not found error
        /// </summary>
        /// <param name="target">target name</param>
        /// <param name="chat"></param>
        /// <returns></returns>
        protected abstract Task<PeerMessage> RespondTargetNotFound(string target, ChatInfo chat);

        /// <inheritdoc cref="Command{T}"/>
        public override bool ClearState(ChatInfo chat) => _currentStep.TryRemove(chat.Id, out _);
    }
}
