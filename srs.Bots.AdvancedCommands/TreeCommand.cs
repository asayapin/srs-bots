﻿using System.Linq;
using srs.Bots.AdvancedCommands.Attributes;
using srs.Bots.AdvancedCommands.Internal;
using srs.Bots.CommandAbstractions;

namespace srs.Bots.AdvancedCommands
{
    /// <summary>
    /// command with tree-style transitions between steps
    /// </summary>
    /// <remarks>
    /// command relies on manual registering of steps or decorating them with <see cref="StepAttribute"/>
    /// also, command requires set of strings indicating transition one step above ("back" transition)
    /// and transition to the tree root ("start" transition)
    /// </remarks>
    public abstract class TreeCommand<T> : GraphCommand<T> where T : CommandDependencies
    {
        /// <summary>
        /// list of commands allowing to navigate back
        /// </summary>
        protected string[] BackCommands;
        /// <summary>
        /// list of commands allowing to navigate to root
        /// </summary>
        protected string[] BackToBeginningCommands;
        /// <summary>
        /// register back commands
        /// </summary>
        /// <param name="routesToBeginning"></param>
        /// <param name="commands"></param>
        protected void AddBackCommands(bool routesToBeginning = false, params string[] commands)
        {
            if (routesToBeginning) BackToBeginningCommands = commands.ToArray();
            else BackCommands = commands.ToArray();
        }
        /// <summary>
        /// default ctor, setting on back navigation in graph command
        /// </summary>
        protected TreeCommand() : base(true, true)
        {
        }
        /// <inheritdoc cref="GraphCommand{T}"/>
        protected override bool TryNavigateBack(string command, CommandGraphNode node, out CommandGraphNode target)
        {
            target = null;
            if (BackToBeginningCommands.Contains(command)) target = GetRoot();
            if (BackCommands.Contains(command)) target = node.Parent;
            return target is null;
        }
    }
}
