﻿using System.Linq;
using System.Threading.Tasks;
using srs.Bots.CommandAbstractions;
using srs.Bots.Data;

namespace srs.Bots.AdvancedCommands
{
    /// <summary>
    /// help navigator command - subtype of tree navigation
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class HelpCommand<T> : TreeCommand<T> where T : CommandDependencies
    {
        /// <summary>
        /// e.g. 'Target {0} not found. Send "{1}" to go back or {2} to start over'
        /// </summary>
        protected readonly string TargetNotFoundFormat;
        /// <inheritdoc cref="GraphCommand{T}"/>
        protected override Task<PeerMessage> RespondTargetNotFound(string target, ChatInfo chat)
        {
            var msg = string.Format(TargetNotFoundFormat, target, BackCommands.FirstOrDefault(),
                BackToBeginningCommands.FirstOrDefault());
            return PeerMessage.FromText(msg, chat).AsTask();
        }
    }
}
