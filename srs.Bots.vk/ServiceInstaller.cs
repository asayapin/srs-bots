﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using srs.Bots.CommandAbstractions;
using srs.Bots.DIAbstractions;
using VkNet;
using VkNet.Abstractions;
using VkNet.Model;

namespace srs.Bots.vk
{
    /// <summary>
    /// vk services installer
    /// </summary>
    public static class ServiceInstaller
    {
        /// <summary>
        /// Installs to the specified services collection.
        /// </summary>
        /// <param name="services">The services.</param>
        /// <param name="accessTokenKey">parameter key for access token</param>
        public static void Install<T, U>(this IServiceCollection services, string accessTokenKey) where U : Command<T> where T : CommandDependencies
        {
            services.UseBotsDI()
                .UsingCommandsAndDependenciesFrom<T>(typeof(U))
                .RespondWith(() => new OkObjectResult("ok"))
                .ParseRequestWith(InternalServices.Parse)
                .AddTo(services);
            services.AddSingleton<IVkApi>(isp =>
            {
                var vk = new VkApi();
                vk.Authorize(new ApiAuthParams{AccessToken = isp.GetService<IConfiguration>()[accessTokenKey]});
                return vk;
            });
        }
    }
}
