﻿using System;
using System.Collections.Generic;
using System.Linq;
using srs.Bots.Data;
using srs.Bots.Utility;
using VkNet.Abstractions;
using VkNet.Enums.SafetyEnums;
using VkNet.Model.Keyboard;
using VkNet.Model.RequestParams;

namespace srs.Bots.vk
{
    /// <summary>
    /// vk-specific extensions
    /// </summary>
    public static class Extensions
    {
        private static IEnumerable<MessageKeyboardButton[]> GetKeyboard(this PeerMessage message)
        {
            return message.GetKeyboard(ToButton);
        }

        private static MessageKeyboardButton ToButton(string s, int i) => s.ToButton(i);

        private static MessageKeyboardButton ToButton(this string s, object payload)
        {
            return new MessageKeyboardButton
            {
                Action = new MessageKeyboardButtonAction
                {
                    Type = KeyboardButtonActionType.Text,
                    Label = s,
                    Payload = payload.ToString()
                },
                Color = KeyboardButtonColor.Primary
            };
        }
        /// <summary>
        /// Sends the peer message.
        /// </summary>
        /// <param name="vk">The vk api helper instance.</param>
        /// <param name="groupId">The group identifier.</param>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        public static long SendPeerMessage(this IVkApi vk, long? groupId, PeerMessage message)
        {
            var @params = new MessagesSendParams
            {
                UserId = message.Recipient,
                RandomId = Random(),
                Message = message.Message
            };
            if (message.Options.Any())
            {
                var keyboard = new MessageKeyboard
                {
                    Inline = false,
                    OneTime = true,
                    Buttons = message.GetKeyboard()
                };
                @params.Keyboard = keyboard;
            }
            if (groupId.HasValue) @params.GroupId = (ulong)groupId.Value;
            return vk.Messages.Send(@params);
        }

        private static int Random()
        {
            return BitConverter.ToInt32(Guid.NewGuid().ToByteArray(), 0);
        }
    }
}
