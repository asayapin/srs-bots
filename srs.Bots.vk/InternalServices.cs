﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;
using srs.Bots.Data;
using srs.Bots.Utility;
using srs.Bots.vk.Inherited;
using srs.Bots.vk.Models;

namespace srs.Bots.vk
{
    internal static class InternalServices
    {
        public static Maybe<UnifiedMessage> Parse(JObject request)
        {
            try
            {
                var type = request.Value<string>("type");
                var message = new UnifiedMessage
                {
                    Key = new VkEventKey { Type = type },
                    OriginalRequest = request,
                    MessageId = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 0)
                };

                var token = request["object"];
                switch (type)
                {
                    case VkEventKey.TypeNewMessage:
                        message.TryFillAsMessage(token);
                        break;
                    case VkEventKey.TypeNewPostReply:
                        message.TryFillAsPost(token);
                        break;
                }

                return Maybe<UnifiedMessage>.FromValue(message);
            }
            catch (Exception ex) { return Maybe<UnifiedMessage>.FromError(ex); }
        }

        private static void TryFillAsMessage(this UnifiedMessage message, JToken obj)
        {
            var msg = obj["message"];
            var text = msg.Value<string>("text");
            var date = msg.Value<long>("date");
            var id = msg.Value<long>("id");
            var from = msg.Value<long>("from_id");
            var payload = msg.Value<string>("payload");
            message.Text = text;
            message.Timestamp = date.FromUnix();
            message.Peer = new ChatInfo{Id = from};
            message.MessageId = id;
            message.Attachments = GetAttachments(msg["attachments"]).ToArray();
            message.LoadData(new Dictionary<string, object>
            {
                ["Payload"] = payload
            });
        }

        private static IEnumerable<Attachment> GetAttachments(JToken @object)
        {
            foreach (var child in @object.Children())
            {
                switch (child["type"].Value<string>())
                {
                    case "photo":
                        var photo = child["photo"];
                        var caption = photo.Value<string>("text");
                        var pic = new VkPicture
                        {
                            OwnerId = photo.Value<long>("owner_id"),
                            AlbumId = photo.Value<long>("album_id"),
                            AccessKey = photo.Value<string>("access_key"),
                            PicId = photo.Value<long>("id"),
                            Url = photo["sizes"]
                                .Children()
                                .GroupBy(x => x.Value<int>("width"))
                                .OrderByDescending(x => x.Key)
                                .FirstOrDefault()
                                ?.FirstOrDefault()
                                ?.Value<string>("url")
                        };
                        yield return new Attachment(caption, pic);
                        break;
                }
            }
        }

        private static void TryFillAsPost(this UnifiedMessage message, JToken token)
        {
            var text = token.Value<string>("text");
            var date = token.Value<long>("date");
            var from = token.Value<long>("from_id");
            var envelope = new Dictionary<string, object>
            {
                ["CommentId"] = token.Value<long>("id"),
                ["PostId"] = token.Value<long>("post_id")
            };
            message.Timestamp = date.FromUnix();
            message.Text = text;
            message.Peer = new ChatInfo{Id = from};
            message.Attachments = GetAttachments(token["attachments"]).ToArray();
            message.LoadData(envelope);
        }

        private static DateTime FromUnix(this long l) => DateTimeOffset.FromUnixTimeSeconds(l).DateTime;
    }
}
