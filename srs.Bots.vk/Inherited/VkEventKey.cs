﻿using srs.Bots.Data;

namespace srs.Bots.vk.Inherited
{
    /// <summary>
    /// vk event key
    /// </summary>
    /// <seealso cref="srs.Bots.Data.EventKey" />
    public class VkEventKey : EventKey
    {
        /// <summary>
        /// Gets or sets the event type.
        /// </summary>
        /// <value>
        /// The event type.
        /// </value>
        public string Type { get; set; }
        /// <summary>
        /// Determines whether this instance is simple (can be handled with general handler) - checks that message encloses text message ('message_new')
        /// </summary>
        /// <returns>
        /// <c>true</c> if this instance is simple; otherwise, <c>false</c>.
        /// </returns>
        public override bool IsSimple()
        {
            return Type == TypeNewMessage;
        }
        /// <summary>
        /// The type 'confirmation'
        /// </summary>
        public const string
            TypeConfirmation = "confirmation";
        /// <summary>
        /// The type 'message_new'
        /// </summary>
        public const string
            TypeNewMessage = "message_new";
        /// <summary>
        /// The type 'wall_reply_new'
        /// </summary>
        public const string
            TypeNewPostReply = "wall_reply_new";
        /// <summary>
        /// The type 'wall_reply_edit'
        /// </summary>
        public const string
            TypeEditPostReply = "wall_reply_edit";
        /// <summary>
        /// The type 'message_edit'
        /// </summary>
        public const string
            TypeEditMessage = "message_edit";
        /// <summary>
        /// The type 'photo_new'
        /// </summary>
        public const string
            TypeNewPhoto = "photo_new";
    }
}
