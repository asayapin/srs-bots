﻿namespace srs.Bots.vk.Models
{
    /// <summary>
    /// picture model for vk
    /// </summary>
    public class VkPicture
    {
        /// <summary>
        /// picture id (from vk)
        /// </summary>
        public long PicId { get; set; }
        /// <summary>
        /// pic album id (vk)
        /// </summary>
        public long AlbumId { get; set; }
        /// <summary>
        /// album owner id (= group id)
        /// </summary>
        public long OwnerId { get; set; }
        /// <summary>
        /// access key for restricted pics
        /// </summary>
        public string AccessKey { get; set; }
        /// <summary>
        /// pic url (for direct access prior to loading in group)
        /// </summary>
        public string Url { get; set; }
    }
}
