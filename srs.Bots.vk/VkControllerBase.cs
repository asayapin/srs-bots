﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using srs.Bots.CommandAbstractions;
using srs.Bots.ControllerAbstractions;
using srs.Bots.Data;
using srs.Bots.vk.Inherited;
using VkNet.Abstractions;

namespace srs.Bots.vk
{
    /// <summary>
    /// vk bot implementation
    /// </summary>
    /// <seealso cref="UnifiedController" />
    public abstract class VkControllerBase : UnifiedController
    {
        /// <summary>
        /// The configuration
        /// </summary>
        protected readonly IConfiguration _config;
        /// <summary>
        /// The vk API
        /// </summary>
        protected readonly IVkApi _vkApi;
        /// <summary>
        /// The quick answer text
        /// </summary>
        public static string QuickAnswer;
        /// <summary>
        /// The inline mismatch format with single placeholder for mismatched text
        /// </summary>
        public static string InlineMismatchFormat;
        /// <summary>
        /// The group identifier (for responses)
        /// </summary>
        protected static long GroupId;
        /// <summary>
        /// config key for group id
        /// </summary>
        protected static string GroupIdKey;
        /// <summary>
        /// Initializes a new instance of the <see cref="VkControllerBase"/> class.
        /// </summary>
        /// <param name="setup">The setup.</param>
        /// <param name="executor">The executor.</param>
        /// <param name="config">The configuration.</param>
        /// <param name="vkApi">The vk API.</param>
        protected VkControllerBase(ControllerSetup setup, IExecutor executor, IConfiguration config, IVkApi vkApi) : base(setup, executor)
        {
            _config = config;
            _vkApi = vkApi;

            GroupId = long.Parse(config[GroupIdKey]);

            Handlers.Add(new VkEventKey{Type = VkEventKey.TypeNewPostReply}, ProcessWallPostReply);
        }
        /// <summary>
        /// handles failed parsing - routes to LogException
        /// </summary>
        /// <param name="ex">The ex.</param>
        protected override void ParseFailedHandler(Exception ex) => LogException(ex);
        /// <summary>
        /// quick answer handler (for messages like 'your query is being processed' until bot warms up) - sends quickAnswer to peer
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        protected override Task QuickAnswerHandler(UnifiedMessage message)
        {
            Executor.InvokeMessageCreated(PeerMessage.FromText(QuickAnswer, message.Peer));
            return Task.CompletedTask;
        }
        /// <summary>
        /// Processes the case message text doesn't match one of present in context's options - sends formatted message to peer
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        protected override Task ProcessInlineMismatch(UnifiedMessage message)
        {
            Executor.InvokeMessageCreated(PeerMessage.FromText(string.Format(InlineMismatchFormat, message.Text), message.Peer));
            return Task.CompletedTask;
        }
        /// <summary>
        /// Processes the plain message (not command, not in context) - does nothing
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        protected override Task ProcessPlainMessage(UnifiedMessage message) => Task.CompletedTask;
        /// <summary>
        /// Processes the wall post reply (special handler).
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        protected abstract Task ProcessWallPostReply(UnifiedMessage message);
    }
}
