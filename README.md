# Bots boilerplate

[![Build status](https://dev.azure.com/asajapin/ci-cd-pipelines/_apis/build/status/srs-bots)](https://dev.azure.com/asajapin/ci-cd-pipelines/_build/latest?definitionId=3)
![Azure DevOps coverage](https://img.shields.io/azure-devops/coverage/asajapin/ci-cd-pipelines/3)
[![docs](https://img.shields.io/badge/docs-gitlab_pages-blue)](https://asayapin.gitlab.io/srs-bots)

A project to help build simple command bots

## Structure

- srs.bots - master package
- srs.bots.vk - vk.com boilerplate
- srs.bots.tlg - telegram boilerplate

To use project, you should reference both srs.bots and the project you want to use

## Usage

*There is example project in the solution*

In general, you should do the following:

- create a controller for bot, inherited from either `UnifiedController`, `VkControllerBase` or `TelegramControllerBase`
- set static fields
  - `BotTokenKey` (tlg) - authentication token for bot
  - `GroupIdKey` (vk) - group id for group bot
  - `InlineMismatchFormat` - for cases you provide user an inline keyboard and he mistakes
  - `QuickAnswer` - for answer prior to full bot warmup
- inherit `CommandDependencies`, possibly add new fields
- inherit `Command` with abstract class
  - hide field `Dependencies`: `protected new MyDependencies Dependencies;`
  - override `SetDependencies` to set `Dependencies` to instance of correct type (which is guaranteed to be correct, thanks to DI)
- create custom command
  - inherit from your local abstract Command inheritor
  - decorate with `CommandAttribute`
  - possibly decorate with `PseudonymsAttribute` for more command pseudonyms
- add injection to `Startup.ConfigureServices`
  - `services.Install<DependenciesType, CommandType>(/*for vk add string: "botAccessToken"*/)`
  
That's it!