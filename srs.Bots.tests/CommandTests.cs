using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NUnit.Framework;
using srs.Bots.CommandAbstractions;
using srs.Bots.Data;
using srs.Bots.tests.Mocks;

namespace srs.Bots.tests
{
    public class CommandTests
    {
        public class TestArgs
        {
            public readonly bool IsEcho;
            public readonly string Expected;
            public string[] ExpectedOptions;
            public CommandContext Context;

            private TestArgs(bool isEcho, string expected)
            {
                IsEcho = isEcho;
                Expected = expected;
            }

            public static TestArgs Echo(string expected) => new TestArgs(true, expected);
            public static TestArgs NotEcho(string expected) => new TestArgs(false, expected);

            public TestArgs Opts(params string[] o)
            {
                ExpectedOptions = o.ToArray();
                return this;
            }

            public TestArgs Ctx(params string[] args)
            {
                Context = new CommandContext("");
                Context.Set(args);
                return this;
            }

            public bool OptionsMatch(IEnumerable<string> options)
            {
                if (options is null && ExpectedOptions is null) return true;
                if (options is null != ExpectedOptions is null) return true;
                var enumerable = options as string[] ?? options.ToArray();
                return enumerable.Length + ExpectedOptions.Length == 0 || enumerable.SequenceEqual(ExpectedOptions);
            }
        }

        private Command<Dependencies> echo, stateful;
        private long chatId;
        private ChatInfo chat;

        [SetUp]
        public void Setup()
        {
            echo = new EchoCommand();
            stateful = new StatefulCommand();
            chat = ChatInfo.FromId(chatId = BitConverter.ToInt64(Guid.NewGuid().ToByteArray(), 0));
        }

        public static IEnumerable<TestCaseData> Cases()
        {
            yield return new TestCaseData(TestArgs.Echo("result").Ctx("result")) { TestName = "echo - 0" };
            yield return new TestCaseData(TestArgs.Echo("result").Ctx("result", "not used")) { TestName = "echo - 1 - unused parameters" };
        }

        [TestCaseSource(nameof(Cases))]
        public async Task Test(TestArgs args)
        {
            var command = args.IsEcho ? echo : stateful;

            var res = await command.ProcessCommand(args.Context, chat);

            Assert.AreEqual(args.Expected, res.Message);
            Assert.IsTrue(args.OptionsMatch(res.Options));
            Assert.AreEqual(chatId, res.Recipient);
        }
    }
}