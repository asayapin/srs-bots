﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using srs.Bots.CommandAbstractions;
using srs.Bots.ControllerAbstractions;
using srs.Bots.Data;

namespace srs.Bots.tests.Mocks
{
    public class Controller : UnifiedController
    {
        public Controller(ControllerSetup setup, IExecutor executor) : base(setup, executor)
        {
        }

        protected override void ParseFailedHandler(Exception resultException)
        {
            throw new NotImplementedException();
        }

        protected override async Task FallbackHandler(UnifiedMessage message)
        {
            throw new NotImplementedException();
        }

        protected override async Task QuickAnswerHandler(UnifiedMessage message)
        {
            throw new NotImplementedException();
        }

        protected override async Task ProcessInlineMismatch(UnifiedMessage message)
        {
            throw new NotImplementedException();
        }

        protected override async Task ProcessPlainMessage(UnifiedMessage message)
        {
            throw new NotImplementedException();
        }

        protected override void LogException(Exception ex)
        {
            throw new NotImplementedException();
        }
    }
}
