﻿using System.Threading.Tasks;
using srs.Bots.CommandAbstractions;
using srs.Bots.Data;

namespace srs.Bots.tests.Mocks
{
    public class EchoCommand : Command<Dependencies>
    {
        public override Task<PeerMessage> ProcessCommand(CommandContext context, ChatInfo chat) => PeerMessage.FromText(context[0], chat).AsTask();
    }
}
