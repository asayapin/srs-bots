﻿using System.Linq;
using System.Text;
using System.Threading.Tasks;
using srs.Bots.Data;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;

namespace srs.Bots.tlg
{
    /// <summary>
    /// telegram-specific extensions
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// Sends the peer message asynchronous.
        /// </summary>
        /// <param name="client">The bot client.</param>
        /// <param name="message">The message.</param>
        public static async Task SendPeerMessageAsync(this TelegramBotClient client, PeerMessage message)
        {
            if (message.IsFile)
                await client.SendPeerDocumentAsync(message);
            else
                await client.SendTextMessageAsync(message.Recipient, message.Message,
                    replyMarkup: message.Options.Any()
                        ? new InlineKeyboardMarkup(message.GetButtons())
                        : null);
        }
        /// <summary>
        /// Sends the peer document asynchronous.
        /// </summary>
        /// <param name="client">The client.</param>
        /// <param name="message">The message.</param>
        public static async Task SendPeerDocumentAsync(this TelegramBotClient client, PeerMessage message)
        {
            var stream = new System.IO.MemoryStream(Encoding.UTF8.GetBytes(message.Message));
            var options = message.GetButtons();
            await client.SendDocumentAsync(
                message.Recipient,
                new FileToSend(message.Caption, stream),
                replyMarkup: options.Length > 0 ? new InlineKeyboardMarkup(options) : null);
        }
        /// <summary>
        /// Gets the buttons for inline keyboard.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        private static InlineKeyboardButton[][] GetButtons(this PeerMessage message) => message.Options.Select((x, i) => new[] { new InlineKeyboardButton(x, i.ToString()) }).ToArray();
    }
}
