﻿using srs.Bots.Data;
using Telegram.Bot.Types.Enums;

namespace srs.Bots.tlg.Inherited
{
    /// <summary>
    /// telegram implementation for EventKey
    /// </summary>
    /// <seealso cref="srs.Bots.Data.EventKey" />
    public class TelegramEventKey : EventKey
    {
        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>
        /// The update type.
        /// </value>
        public UpdateType Type { get; set; }
    }
}
