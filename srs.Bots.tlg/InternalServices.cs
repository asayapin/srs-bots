﻿using System;
using System.Linq;
using Newtonsoft.Json.Linq;
using srs.Bots.Data;
using srs.Bots.tlg.Inherited;
using srs.Bots.Utility;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace srs.Bots.tlg
{
    /// <summary>
    /// internal telegram services
    /// </summary>
    internal static class InternalServices
    {
        /// <summary>
        /// Parses the request body json.
        /// </summary>
        /// <param name="arg">The argument.</param>
        /// <returns></returns>
        public static Maybe<UnifiedMessage> Parse(JObject arg)
        {
            try
            {
                var update = arg["result"] is { } res ? res.First().ToObject<Update>() : arg.ToObject<Update>();
                var message = new UnifiedMessage
                {
                    MessageId = update.Id,
                    Key = new TelegramEventKey { Type = update.Type },
                    OriginalRequest = arg,
                    Attachments = new Attachment[0]
                };

                switch (update.Type)
                {
                    case UpdateType.MessageUpdate:
                        message.FromMessage(update.Message);
                        break;
                    case UpdateType.InlineQueryUpdate:
                        message.FromInlineQuery(update.InlineQuery);
                        break;
                    case UpdateType.ChosenInlineResultUpdate:
                        message.FromInlineResult(update.ChosenInlineResult);
                        break;
                    case UpdateType.CallbackQueryUpdate:
                        message.FromCallbackQuery(update.CallbackQuery);
                        break;
                }

                return Maybe<UnifiedMessage>.FromValue(message);
            }
            catch (Exception e)
            {
                return Maybe<UnifiedMessage>.FromError(e);
            }
        }

        private static void FromMessage(this UnifiedMessage um, Message msg)
        {
            um.MessageId = msg.MessageId;
            um.Peer = new ChatInfo
            {
                Id = msg.Chat.Id,
                Name = msg.Chat.Username
            };
            um.Timestamp = msg.Date;
            um.Text = msg.Text ?? msg.Caption;
            if (new[]
            {
                MessageType.AudioMessage, MessageType.PhotoMessage, MessageType.VideoMessage,
                MessageType.DocumentMessage, MessageType.VoiceMessage
            }.Contains(msg.Type))
                um.Attachments = new[]
                {
                    new Attachment(msg.Caption,
                        msg.Photo ?? msg.Document ?? msg.Audio ?? (object) msg.Video ?? msg.Voice)
                };
        }

        private static void FromInlineQuery(this UnifiedMessage um, InlineQuery msg)
        {
            um.MessageId = long.TryParse(msg.Id, out var l) ? l : 0;
            um.Peer = new ChatInfo
            {
                Id = msg.From.Id,
                Name = msg.From.Username
            };
            um.Text = msg.Query;
        }

        private static void FromInlineResult(this UnifiedMessage um, ChosenInlineResult msg)
        {
            um.MessageId = long.TryParse(msg.InlineMessageId, out var l) ? l : 0;
            um.Peer = new ChatInfo
            {
                Id = msg.From.Id,
                Name = msg.From.Username
            };
            um.Text = msg.Query;
        }

        private static void FromCallbackQuery(this UnifiedMessage um, CallbackQuery msg)
        {
            um.MessageId = long.TryParse(msg.Id, out var l) ? l : 0;
            um.Peer = new ChatInfo
            {
                Id = msg.From.Id,
                Name = msg.From.Username
            };
            um.Text = msg.Data;
        }
    }
}
