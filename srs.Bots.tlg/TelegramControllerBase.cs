﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using srs.Bots.CommandAbstractions;
using srs.Bots.ControllerAbstractions;
using srs.Bots.Data;
using srs.Bots.tlg.Inherited;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace srs.Bots.tlg
{
    /// <summary>
    /// unified controller implementation for telegram
    /// </summary>
    /// <seealso cref="srs.Bots.ControllerAbstractions.UnifiedController" />
    public abstract class TelegramControllerBase : UnifiedController
    {
        private static bool IsInit = false;

        /// <summary>
        /// key to get bot auth token from configuration
        /// </summary>
        public static string BotTokenKey;

        /// <summary>
        /// quick answer text
        /// </summary>
        public static string QuickAnswer;

        /// <summary>
        /// inline mismatch answer format, with single placeholder for mismatched value
        /// </summary>
        public static string InlineMismatchFormat;
        /// <summary>
        /// The bot client - to interact with telegram
        /// </summary>
        protected static TelegramBotClient Client;
        /// <summary>
        /// Initializes a new instance of the <see cref="TelegramControllerBase"/> class.
        /// </summary>
        /// <param name="setup">The setup.</param>
        /// <param name="executor">The executor.</param>
        /// <param name="config">The configuration.</param>
        protected TelegramControllerBase(ControllerSetup setup, IExecutor executor, IConfiguration config) : base(setup, executor)
        {
            Client = new TelegramBotClient(config[BotTokenKey]);
            if (!IsInit)
            {
                Executor.MessageCreated += async m => await Client.SendPeerMessageAsync(m);
                IsInit = true;
            }
        }
        /// <summary>
        /// handle case of failed request parsing - route to <see cref="UnifiedController.LogException"/>
        /// </summary>
        /// <param name="resultException">parse exception</param>
        protected override void ParseFailedHandler(Exception resultException) => LogException(resultException);
        /// <summary>
        /// quick answer handler (for messages like 'your query is being processed' until bot warms up)
        /// </summary>
        /// <param name="message">The message.</param>
        protected override async Task QuickAnswerHandler(UnifiedMessage message) => await Client.SendPeerMessageAsync(PeerMessage.FromText(QuickAnswer, message.Peer));
        /// <summary>
        /// Processes the case message text doesn't match one of present in context's options
        /// </summary>
        /// <param name="message">The message.</param>
        protected override async Task ProcessInlineMismatch(UnifiedMessage message)
        {
            var text = string.Format(InlineMismatchFormat, message.Text);
            await Client.SendPeerMessageAsync(PeerMessage.FromText(text, message.Peer));
        }
        /// <summary>
        /// Processes the plain message (not command, not in context) - does nothing
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        protected override Task ProcessPlainMessage(UnifiedMessage message) => Task.CompletedTask;
        /// <summary>
        /// Preprocess the message - find inline option for CallbackQueryUpdate and set as message text
        /// </summary>
        /// <param name="message">The message.</param>
        protected override void PreProcessMessage(UnifiedMessage message)
        {
            if ((message.Key as TelegramEventKey)?.Type is UpdateType.CallbackQueryUpdate)
            {
                if (TryGetContext(message.Peer.Id, out var ctx) && int.TryParse(message.Text, out var option))
                {
                    ctx.Add(true, ctx[option, false]);
                    ctx.Set(false);
                    message.Text = "";
                }
                try
                {
                    var messageId = message.OriginalRequest.ToObject<Update>().CallbackQuery.Message.MessageId;
                    var _ = Client.EditMessageReplyMarkupAsync(message.Peer.Id, messageId).Result;
                }
                catch (Exception ex)
                {
                    LogException(ex);
                }
            }
            else
                base.PreProcessMessage(message);
        }
        /// <summary>
        /// process string with, possibly, command name - trim special symbols, etc - trim leading slashes
        /// </summary>
        /// <param name="s">The string.</param>
        /// <returns></returns>
        protected override string PrepareCommandName(string s)
        {
            return s.TrimStart('/');
        }
    }
}
