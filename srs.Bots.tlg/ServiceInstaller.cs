﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using srs.Bots.CommandAbstractions;
using srs.Bots.DIAbstractions;

namespace srs.Bots.tlg
{
    /// <summary>
    /// telegram service installer
    /// </summary>
    public static class ServiceInstaller
    {
        /// <summary>
        /// Installs to the specified services.
        /// </summary>
        /// <param name="services">The services.</param>
        /// <typeparam name="TDependencies">dependencies implementation type</typeparam>
        /// <typeparam name="TCommand">command implementation type (for which to scan containing assembly)</typeparam>
        public static void Install<TDependencies, TCommand>(this IServiceCollection services) where TDependencies : CommandDependencies where TCommand : Command<TDependencies>
        {
            services.UseBotsDI()
                .UsingCommandsAndDependenciesFrom<TDependencies>(typeof(TCommand))
                .RespondWith(() => new OkResult())
                .ParseRequestWith(InternalServices.Parse)
                .AddTo(services);
        }
    }
}
